<?php

namespace laylatichy\nano\core;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use JetBrains\PhpStorm\Pure;

final class Validate {
    public static function email(string $email): bool {
        $validator = new EmailValidator();

        $multipleValidations = new MultipleValidationWithAnd(
            validations: [
                new RFCValidation(),
                new DNSCheckValidation(),
            ]
        );

        return $validator->isValid(email: $email, emailValidation: $multipleValidations);
    }

    #[Pure]
    public static function token(string $tokenRequired, string $tokenReceived): bool {
        return $tokenRequired === $tokenReceived;
    }
}
