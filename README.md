### nano/core/validate

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-validate?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-validate)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-validate?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-validate)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/validate/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/validate/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/validate/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/validate/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/validate/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/validate/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-validate`
